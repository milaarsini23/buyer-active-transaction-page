import 'package:flutter/material.dart';


void main() {
  runApp(new MaterialApp(
    title: "Parsing Data",
    home: new Checkout(),
  ));
}

class Checkout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
     appBar: new AppBar(title: new Text("Checkout"), leading: new Icon(Icons.arrow_back, color: Colors.white)),
      body: Column(
          children: <Widget>[
            new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Image.network('https://www.pngitem.com/pimgs/m/23-235870_google-location-icon-png-location-symbol-red-png.png', height: 25 ),
                Text('Alamat Pengiriman',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Pilih Alamat Lain >', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.blue),),

                ],
              ) ,
            ),
             new Card(
              child: new Column(
                children: <Widget>[
                Text('Tiara|081234567897', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                 Text('Jl.Samratulangi No.30 Penarukan', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('KAB. BULELENG-BULELENG BALI ID', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
              ],
            ) ,
          ),
            new Card(
              child: new Row(
                children: <Widget>[
                Text('Star+',
                  style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0),
                ),
                Text('Wyth_Shop', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),),
                ],
              ) ,
            ),
             new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Image.network('https://id-test-11.slatic.net/p/b21fc180ebd8e1d8fbfb720ee57c2efe.jpg_200x200q90.jpg_.webp', height: 80 ),
                new Card(
                  child: new Column(
                  children:<Widget> [
                    Text('Paketan Votre Keyboard USB KB2308', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),),
                    Text('Rp.589.899', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),),
                  ],
                ),
              ),
            ],
          ) ,
        ),
         new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Text('Opsi Pengiriman',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Pilih Opsi Lain >', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.blue),),
                ],
              ) ,
            ),
          new Card(
              child: new Column(
                children: <Widget>[
                Text('Reguler',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Akan diterima sebelum 11-14 Juni', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),),
                new Card(
                  child: new Row(
                  children:<Widget> [Text('Rp.30.000 >', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),),
                  ],
                ),
              ),
            ],
          ) ,
        ),
         new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Text('Total Pesanan (1 Produk)',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Rp.589.899', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0),),
                ],
              ) ,
            ),
           new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Text('Pesan',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Tulis Pesan Kepada Penjual >', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.blue),),
                ],
              ) ,
            ),
          new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Image.network('https://img.icons8.com/dotty/2x/discount-ticket.png', height: 30 ),
                Text('voucher',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Gunakan/masukkan kode voucher', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),),
                ],
              ) ,
            ),
           new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Image.network('https://icons-for-free.com/iconfiles/png/512/money+payment+icon-1320165997481413640.png', height: 45 ),
                Text('Metode Pembayaran',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Pilih Metode Pembayaran >', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),),
                ],
              ) ,
            ),
            new Card(
              child: new Row(
                children: <Widget>[
                Text('Sub Total Produk',
                  style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),
                ),
                Text('Sub Total Pengiriman',
                    style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),
                    ),
                Row(
                  children:<Widget> [
                    Text('Rp.589.899', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),),
                    Text('Rp.30.000 >', style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),),
                  ],
                ),
              ],
            ) ,
          ),
             new Card(
              child: new Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                Text('Total Pembayaran',
                  style: TextStyle(fontSize: 14, fontFamily: "Serif", height: 2.0),
                ),
                Text('Rp.619.899', style: TextStyle(fontSize: 13, fontFamily: "Serif", height: 2.0, color: Colors.grey),),
                ],
              ) ,
            ),
          ],
        ),
      );      
    }
}

