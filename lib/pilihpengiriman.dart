import 'package:flutter/material.dart';

class pilihPengiriman {
  int id;
  String name;
  String est;

  pilihPengiriman(this.id, this.name, this.est);
}

List<pilihPengiriman> data = [
  pilihPengiriman(1, "YES (1-2 hari)", "estimasi tiba "),
  pilihPengiriman(2, "Reguler (3-5 hari)", "estimasi tiba "),
  pilihPengiriman(3, "Ekonomi (6-10 hari)", "estimasi tiba "),
];

class ItemUser extends StatelessWidget {
  final int index;
  final pilihPengiriman data;
  final void Function(pilihPengiriman) onClick;

  ItemUser(this.index, this.data, this.onClick);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24, left: 16, right: 16),
      child: InkWell(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CircleAvatar(
                radius: 24,
                backgroundColor: Colors.lightBlue,
                child: Container(
                  margin: EdgeInsets.only(left: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        data.name,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 2),
                        child: Text(data.est),
                      ),
                    ],
                  ),
                )),
          ],
        ),
        onTap: () {
          onClick(data);
        },
      ),
    );
  }
}


